
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		// Ganzzahl
		System.out.printf("|%+-10d%+10d|\n", 123456, -123456);
		
		// Kommazahlen
		System.out.printf("|%10.2f|\n", 12.123456789);
		System.out.printf("|%-10.2f|\n", 12.123456789);
		System.out.printf("|%+-10.2f|\n", 12.123456789);
		
		//Zeichenketten
		System.out.printf("|%s|\n","Max Mustermann");
		System.out.printf("|%10s|\n","Max Mustermann");
		System.out.printf("|%-10.3s|\n","Max Mustermann");
		
		System.out.printf("Name:%-10s  Alter:%-8d  Gewicht:%10.2f|\n","Max", 18, 80.50);
		
		

	}

}
